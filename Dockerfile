FROM ubuntu:focal

# Set terminal to be noninteractive
ENV DEBIAN_FRONTEND noninteractive

# Damned, infernal sources time/bandwidth wastage!
RUN sed -i 's/deb-src/# deb-src/' /etc/apt/sources.list

# Put main packages in place
RUN apt-get update && \
    apt-get upgrade -y -f && \
    apt-get install --no-install-recommends --allow-unauthenticated -y -f \
        sudo \
        curl \
		gnupg \
		ca-certificates

# ChirpStack software repository
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 1CE2AFD36DBCCA00
RUN	echo "deb https://artifacts.chirpstack.io/packages/3.x/deb stable main" > /etc/apt/sources.list.d/chirpstack.list && \
	apt-get update

# Put software packages in place
RUN apt-get install --no-install-recommends --allow-unauthenticated -y -f \
		mosquitto \
		mosquitto-clients \
		apt-transport-https \
		dirmngr \
		redis-tools \
		postgresql \
		net-tools \
		inetutils-telnet \
		supervisor \
		vim \
		# install chirpstack-gateway-bridge
		chirpstack-gateway-bridge \
		# install chirpstack-network-server
		chirpstack-network-server \
		#install chirpstack-application-server
		chirpstack-application-server && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# copy configs
ADD config/chirpstack/ /app/data/chirpstack/
RUN mkdir -p /app/data/chirpstack-gateway-bridge && \
	rm -rf /etc/chirpstack-gateway-bridge && \
	ln -s /app/data/chirpstack-gateway-bridge /etc/chirpstack-gateway-bridge && \
	mkdir -p /app/data/chirpstack-network-server && \
	rm -rf /etc/chirpstack-network-server && \
	ln -s /app/data/chirpstack-network-server /etc/chirpstack-network-server && \
	mkdir -p /app/data/chirpstack-application-server && \
	rm -rf /etc/chirpstack-application-server && \
	ln -s /app/data/chirpstack-application-server /etc/chirpstack-application-server

#ADD config/mosquitto /app/data/mosquitto/config
RUN rm -rf /var/log/mosquitto/mosquitto.log && \
	touch /run/mosquitto.log && \
	chown -R mosquitto:mosquitto /run/mosquitto.log && \
	ln -s  /run/mosquitto.log /var/log/mosquitto/mosquitto.log

# generate a random secret
#RUN export RANDOM_SECRET=$(openssl rand -base64 32) && \
#	sed -i "s;verysecret;${RANDOM_SECRET};" /app/data/chirpstack/chirpstack-application-server.toml

EXPOSE 1700/udp
EXPOSE 80

# supervisor
# ADD config/supervisor/ /etc/supervisor/conf.d/
ADD config/supervisor/ /app/data/supervisor/
RUN rm -rf /etc/supervisor/supervisord.conf && \
    rm -rf /etc/supervisor/conf.d/ && \
    ln -sf /app/data/supervisor/supervisord.conf /etc/supervisor/supervisord.conf && \
    ln -sf /app/data/supervisor/conf.d /etc/supervisor/conf.d && \
    ln -sf /run/supervisord.log /var/log/supervisor/supervisord.log

COPY start.sh /app/code/

CMD [ "/app/code/start.sh" ]
