-- set up the users and the passwords
-- (note that it is important to use single quotes and a semicolon at the end!)
create role db_chirpstack_as NOLOGIN NOINHERIT;
create role user_chirpstack_as with login password 'DB_PASSWORD_1' IN ROLE db_chirpstack_as;

create role db_chirpstack_ns NOLOGIN NOINHERIT;
create role user_chirpstack_ns with login password 'DB_PASSWORD_2' IN ROLE db_chirpstack_ns;

-- create the database for the servers
create database db_chirpstack_ns with owner user_chirpstack_ns;
grant all privileges on database db_chirpstack_ns to user_chirpstack_ns;

create database db_chirpstack_as with owner user_chirpstack_as;
grant all privileges on database db_chirpstack_as to user_chirpstack_as;

-- change to the ChirpStack Application Server database
\c db_chirpstack_as

-- enable the pq_trgm and hstore extensions
-- (this is needed to facilitate the search feature)
create extension pg_trgm;
-- (this is needed to store additional k/v meta-data)
create extension hstore;

