#!/bin/bash

if [[ ! -d /app/data/default ]]
then

#    echo "Initializing database..."
#
#PGPASSWORD=${CLOUDRON_POSTGRESQL_PASSWORD} psql -h${CLOUDRON_POSTGRESQL_HOST} -p${CLOUDRON_POSTGRESQL_PORT} -U${CLOUDRON_POSTGRESQL_USERNAME} <<-EOSQL
#    create database ${CLOUDRON_POSTGRESQL_DATABASE}_chirpstack_ns with owner ${CLOUDRON_POSTGRESQL_USERNAME};
#    grant all privileges on database db_chirpstack_ns to user_chirpstack_ns;
#    create database ${CLOUDRON_POSTGRESQL_DATABASE}_chirpstack_as with owner ${CLOUDRON_POSTGRESQL_USERNAME};
#    grant all privileges on database db_chirpstack_as to user_chirpstack_as;
#EOSQL
#PGPASSWORD=${CLOUDRON_POSTGRESQL_PASSWORD} psql -h${CLOUDRON_POSTGRESQL_HOST} -p${CLOUDRON_POSTGRESQL_PORT} -U${CLOUDRON_POSTGRESQL_USERNAME} -d${CLOUDRON_POSTGRESQL_DATABASE}_chirpstack_as <<-EOSQL
#    create extension pg_trgm;
#    create extension hstore;
#EOSQL

#    cat /app/data/config/sql/db-create.sql \
#        | sed -e "s;role chirpstack_;role ${CLOUDRON_POSTGRESQL_USERNAME}_chirpstack_;g" \
#        | sed -e "s;owner chirpstack_;owner ${CLOUDRON_POSTGRESQL_USERNAME}_chirpstack_;g" \
#        | sed -e "s; chirpstack_; ${CLOUDRON_POSTGRESQL_USERNAME}_chirpstack_;g" \
#        | sed -e "s;dbpassword;${CLOUDRON_POSTGRESQL_PASSWORD};g" \
#        | PGPASSWORD=${CLOUDRON_POSTGRESQL_PASSWORD} psql -h ${CLOUDRON_POSTGRESQL_HOST} -p ${CLOUDRON_POSTGRESQL_PORT} -U ${CLOUDRON_POSTGRESQL_USERNAME}
#    if [ $? -ne 0 ]; then
#        echo "Unable to init database."
#        exit 1
#    fi

    export CLOUDRON_MOSQUITTO_HOST="127.0.0.1"

    if [[ ! -f /app/data/chirpstack-gateway-bridge/chirpstack-gateway-bridge.toml ]]
    then
        cat /app/data/chirpstack/chirpstack-gateway-bridge.toml |
            sed -e "s;mosquitto;${CLOUDRON_MOSQUITTO_HOST};" > /app/data/chirpstack-gateway-bridge/chirpstack-gateway-bridge.toml
    fi

    # put posgresql & redis details in chirpstack-network-server config
    if [[ ! -f /app/data/chirpstack-network-server/chirpstack-network-server.toml ]]
    then
        cat /app/data/chirpstack/chirpstack-network-server.toml |
            sed -e "s;postgresql_host;${CLOUDRON_POSTGRESQL_HOST};" |
            sed -e "s;postgresql_port;${CLOUDRON_POSTGRESQL_PORT};" |
#            sed -e "s;user_chirpstack_ns;${CLOUDRON_POSTGRESQL_USERNAME};" |
#            sed -e "s;password_chirpstack_ns;${CLOUDRON_POSTGRESQL_PASSWORD};" |
#            sed -e "s;database_chirpstack_ns;${CLOUDRON_POSTGRESQL_DATABASE}_chirpstack_ns;" |
            sed -e "s;redis_url;${CLOUDRON_REDIS_URL};" |
            sed -e "s;mosquitto;${CLOUDRON_MOSQUITTO_HOST};" > /app/data/chirpstack-network-server/chirpstack-network-server.toml
    fi

    # put posgresql & redis details in chirpstack-application-server config
    if [[ ! -f /app/data/chirpstack-application-server/chirpstack-application-server.toml ]]
    then
        # generate a random secret
        export RANDOM_SECRET=$(openssl rand -base64 32) && \
            sed -i "s;verysecret;${RANDOM_SECRET};" /app/data/chirpstack/chirpstack-application-server.toml

        cat /app/data/chirpstack/chirpstack-application-server.toml |
            sed -e "s;postgresql_host;${CLOUDRON_POSTGRESQL_HOST};" |
            sed -e "s;postgresql_port;${CLOUDRON_POSTGRESQL_PORT};" |
#            sed -e "s;user_chirpstack_as;${CLOUDRON_POSTGRESQL_USERNAME};" |
#            sed -e "s;password_chirpstack_as;${CLOUDRON_POSTGRESQL_PASSWORD};" |
#            sed -e "s;database_chirpstack_as;${CLOUDRON_POSTGRESQL_DATABASE}_chirpstack_as;" |
            sed -e "s;redis_url;${CLOUDRON_REDIS_URL};" |
            sed -e "s;mosquitto;${CLOUDRON_MOSQUITTO_HOST};" > /app/data/chirpstack-application-server/chirpstack-application-server.toml
    fi

    # create this is first install, so setup /app/data and initial settings
    mkdir -p /app/data/default
fi

echo "Starting ChirpStack"

exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i Chirpstack
