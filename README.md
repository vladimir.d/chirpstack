# ChirpStack Docker example

This repository contains a skeleton to setup the [ChirpStack](https://www.chirpstack.io)
open-source LoRaWAN Network Server stack at Cloudron

## Configuration

The ChirpStack stack components components are pre-configured to work with the provided
`Dockerfile.cloudron` file and defaults to the EU868 LoRaWAN band.

# Data persistence

PostgreSQL and Redis data used from Cloudron Services

## Requirements

Before deploying ChirpStack Server at Cloudron you should create two databases:
`db_chirpstack_as` and `db_chirpstack_ns` for Chirpstack Application and Network Servers.

1. to create databases in Postgresql generate two strong passwords to access databases.
( when `multipleDatabases` option of `postgresql` addon is implemented at Cloudron,
this step would be redundant ) 

```
$ openssl rand -base64 64
```

2. Create databases (replace DB passwords with generated ones)

```
-- set up the users and the passwords
-- (note that it is important to use single quotes and a semicolon at the end!)
create role db_chirpstack_as NOLOGIN NOINHERIT;
create role user_chirpstack_as with login password 'PASSWORD_1' IN ROLE db_chirpstack_as;

create role db_chirpstack_ns NOLOGIN NOINHERIT;
create role user_chirpstack_ns with login password 'PASSWORD_2' IN ROLE db_chirpstack_ns;

-- create the database for the servers
create database db_chirpstack_ns with owner user_chirpstack_ns;
grant all privileges on database db_chirpstack_ns to user_chirpstack_ns;

create database db_chirpstack_as with owner user_chirpstack_as;
grant all privileges on database db_chirpstack_as to user_chirpstack_as;

-- change to the ChirpStack Application Server database
\c db_chirpstack_as

-- enable the pq_trgm and hstore extensions
-- (this is needed to facilitate the search feature)
create extension pg_trgm;
-- (this is needed to store additional k/v meta-data)
create extension hstore;

-- exit psql
\q
```

3. specify passwords in Chirpstack application and network servers configs.
Namely, replace
	`password_chirpstack_as` in `config/chirpstack/chirpstack-application-server.toml` and
	`password_chirpstack_ns` in `config/chirpstack/chirpstack-network-server.toml`

```
$ sed -i "s;password_chirpstack_as;PASSWORD_1;" config/chirpstack/chirpstack-application-server.toml
$ sed -i "s;password_chirpstack_ns;PASSWORD_2;" config/chirpstack/chirpstack-network-server.toml
```

## Usage

To deploy the ChirpStack open-source LoRaWAN Network Server stack, simply run:

```
$ Tag=cloudron-$(date +%s) && \
    docker build -f Dockerfile.cloudron --no-cache .
		-t registry.gitlab.com/brandlight/cloudron/chirpstack:$Tag && \
    docker push registry.gitlab.com/brandlight/cloudron/chirpstack:$Tag && \
    cloudron install --no-wait --server YOUR_CLOUDRON_SERVER --location chirpstack \
		--image registry.gitlab.com/brandlight/cloudron/chirpstack:$Tag \
		--token GITLAB_TOKEN && \
    docker rmi -f registry.gitlab.com/brandlight/cloudron/chirpstack:$Tag
```

**Note:** during the startup of services, it is normal to see the following errors:

* ping database error, will retry in 2s: dial tcp 172.20.0.4:5432: connect: connection refused
* ping database error, will retry in 2s: pq: the database system is starting up


After all the components have been initialized and started, you should be able
to open https://chirpstack.YOUR_CLOUDRON_SERVER/ in your browser.

### Add Network Server

When adding the Network Server in the ChirpStack Application Server web-interface
(see [Network Servers](https://www.chirpstack.io/application-server/use/network-servers/)),
you must enter `chirpstack-network-server:8000` as the Network Server `hostname:IP`.
