This app is pre-setup with an admin account. The initial credentials are:

Username: admin

Password: admin

Please change the admin password immediately.

Note: during the startup of services, it is normal to see the following errors:

ping database error, will retry in 2s: dial tcp 172.20.0.4:5432: connect: connection refused
ping database error, will retry in 2s: pq: the database system is starting up
After all the components have been initialized and started, you should be able to open http://localhost:8080/ in your browser.

Add Network Server

When adding the Network Server in the ChirpStack Application Server web-interface (see Network Servers), you must enter chirpstack-network-server:8000 as the Network Server hostname:IP.
